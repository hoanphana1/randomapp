package com.hoanphan.myapp1;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvKq;
    EditText nhapa;
    EditText nhapb;
    Button btnRandom;
    Button btnExit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nhapa = findViewById(R.id.edta) ;
        nhapb = findViewById(R.id.edtb) ;
        tvKq = findViewById(R.id.tvKQ) ;
        btnRandom = findViewById(R.id.mybutton);
        btnExit = findViewById(R.id.butExit);
        btnRandom.setOnClickListener(this);
        btnExit.setOnClickListener(this);
    }

   @SuppressLint("SetTextI18n")
   @Override
    public void onClick(View v) {
        {
            nhapa = findViewById(R.id.edta) ;
            int a = Integer.parseInt(nhapa.getText()+"");
            nhapb = findViewById(R.id.edtb) ;
            int b = Integer.parseInt(nhapb.getText()+"");
            if(v != btnExit){
                if(b <= a){
                    AlertDialog.Builder d = new AlertDialog.Builder(this);
                    d.setTitle("Exception..!!!");
                    d.setMessage("Input not invalid a>b?");
                    d.setCancelable(false);
                    d.setPositiveButton("Retype", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface d, int arg1) {
                            nhapa.setText("");
                            nhapb.setText("");
                            tvKq.setText("");
                        }
                    });
                    d.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface d, int which) {
                           finish();
                        }
                    });
                    AlertDialog alertDialog = d.create();
                    alertDialog.show();
                }else{
                tvKq =  findViewById(R.id.tvKQ) ;
                Random r = new Random();
                String kq = r.nextInt(b-a)+a+"";
                tvKq.setText(kq);}

            }
            if(v == btnExit){
               finish();
            }
        }

    }
}